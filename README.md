# Utils_Collection

## About This Repo
This is a simple repository which stores all utilities I built myself and it is built for fun and exercising.

## Utils List 

### Completed
- [`calc`](/calc)

### Planned
- `aplay`, play any audio through command line by using an audio playback library (not yet defined)
- `filecat`, Process file contents, output file contents to specific location, main features including clear, read, write file's content
