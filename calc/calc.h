#ifndef calc_h
#define calc_h
#endif

// Addition
long add(long a,long b);

// Subtract
long minus(long a,long b);

// Multiplication
long multi(long a,long b);

// Division
double divid(double a,double b);

// Total Area
long area(long a,long b);

// Total Volume
long vol(long a,long b,long c);