#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// Calculations header
#include "calc.h"

void main(int argc, char *argv[]){
	// current error: produce segmentation fault while launched without arguments
	char *err1;

	char *err2;

	char *inval;

	char *help_msg;

	// ptr1: for strtol() uses only
	char *ptr1;

	err1 = "Arguments isn't enough!(Require 2)\n";

	err2 = "Arguments is too much!(Only require 2)\n";

	inval = "Invalid arguments provided.\n";

	help_msg = " \
	\n \
	Command Line Guide \n \
	\n \
	(val_n = value)\n \
	\n \
	add val_1 val_2 - Addition\n \
	\n \
	minus val_1 val_2 - Subtraction\n \
	\n \
	multiply val_1 val_2 - Multiplication\n \
	\n \
	divide val_1 val_2 - Division\n \
	\n \
	area of val_1 val_2 - Total Area (val_1*val_2)\n \
	\n \
	volume of val_1 val_2 val_3 - Total Volume (val_1*val_2*val_3)\n\n";

	if (argc > 1) {
		if (strcmp(argv[1], "add") == 0) {
			if (argc == 4) {
				long value1 = strtol(argv[2], &ptr1, 10);
				long value2 = strtol(argv[3], &ptr1, 10);
				long ans = add(value1, value2);
				printf("%d\n", ans);

			} else {
				printf("Arguments isn't enought or too much! (Requires 4)\n");
			}
		}

		else if (strcmp(argv[1], "minus") == 0) {
			if (argc == 4) {
				long value1 = strtol(argv[2], &ptr1, 10);
				long value2 = strtol(argv[3], &ptr1, 10);
				long ans = minus(value1, value2);
				printf("%d\n", ans);

			} else {
				printf("Arguments isn't enough or too much! (Requires 4)\n");
			}
		}

		else if (strcmp(argv[1], "multiply") == 0) {
			if (argc == 4) {
				long value1 = strtol(argv[2], &ptr1, 10);
				long value2 = strtol(argv[3], &ptr1, 10);
				long ans = multi(value1, value2);
				printf("%d\n", ans);

			} else {
				printf("Arguments isn't enough or too much! (Requires 4)\n");
			}
		}

		else if (strcmp(argv[1], "divide") == 0) {
			if (argc == 4) {
				double value1 = strtod(argv[2], &ptr1);
				double value2 = strtod(argv[3], &ptr1);
				double ans = divid(value1, value2);
				printf("%f\n", ans);

			} else {
				printf("Arguements isn't enough or too much! (Requires 4)\n");
			}
		}

		else if (strcmp(argv[1], "area")) {
			if (argc == 5) {
				if ( strcmp(argv[2], "of") != 0) {
					printf("Argument 'area' must be followed by 'of'\n");

				} else {
					long l = strtol(argv[3], &ptr1, 10);
					long h = strtol(argv[4], &ptr1, 10);
					long ans = multi(l, h);
					printf("%d\n", ans);
				}

			} else {
				printf("Arguments isn't enough or too much! (Requires 5)\n");
			}
		}

		else if (strcmp(argv[1], "volume")) {
			if (argc == 6) {
				if ( strcmp(argv[2], "of") != 0) {
					printf("Argument 'volume' must be followed by 'of'\n");

				} else {
					long l = strtol(argv[3], &ptr1, 10);
					long w = strtol(argv[4], &ptr1, 10);
					long h = strtol(argv[5], &ptr1, 10);
					long ans = vol(l, w, h);
					printf("%d\n", ans);

				}

			} else {
				printf("Arguments isn't enough or too much! (Requires 6)\n");
			}
		}


	} else {
		printf("%s", help_msg);
	}
}